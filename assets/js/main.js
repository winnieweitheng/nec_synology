(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var selector = "#windows-container";

var counter = 0;

var AddRemoveApplication = function () {
    function AddRemoveApplication() {
        _classCallCheck(this, AddRemoveApplication);

        if ($(selector).length == 0) return;
        this.init();
    }

    _createClass(AddRemoveApplication, [{
        key: "init",
        value: function init() {
            // $('.icon-to-click').dblclick(function () {
            //     $('.window').removeClass('d-none');
            // })
            var dataURL = 'dummy-data/application.json';
            var app = new Vue({
                el: selector,
                data: {
                    applications: [] // initialize empty array
                },
                mounted: function mounted() {
                    // START: Get Application from JSON
                    var self = this;
                    $.getJSON(dataURL, function (data) {
                        self.applications = data;
                    });
                    // END: Get Application from JSON

                    // START: Drag and Drop Application
                    setTimeout(function () {
                        $(".draggable").draggable({
                            cursor: 'move',
                            revert: 'invalid',
                            helper: function helper() {
                                var cloned = $(this).clone();
                                return cloned;
                            },
                            start: function start() {
                                $(".main-menu ").attr("style", "overflow: visible;");
                                // console.log($(selector).find('.main-menu').height());
                                $.each($('.application-wrapper').find('.application-item'), function (i, e) {
                                    console.log($(e).position().top + '>' + $(selector).find('.main-menu').height());
                                    if ($(e).position().top > $(selector).find('.main-menu').height()) {
                                        $(this).addClass('d-none');
                                    }
                                });
                            },
                            stop: function stop() {
                                $(".main-menu ").attr("style", "overflow: scroll;");
                                $('.application-wrapper').find('.application-item').removeClass('d-none');
                            }
                        });

                        $(".droppable").droppable({
                            //hoverClass: 'ui-state-active',
                            tolerance: 'pointer',
                            accept: function accept(event, ui) {
                                return true;
                            },
                            drop: function drop(event, ui) {
                                var obj;
                                if ($(ui.helper).hasClass('draggable')) {
                                    obj = $(ui.helper).clone();
                                    var id_elem = ui.helper[0].id;
                                    obj.removeClass('draggable').addClass('editable').removeAttr('style');
                                    obj.addClass(id_elem);

                                    if ($('#editor-container').find('.' + id_elem).hasClass('editable')) {} else {

                                        $(this).append(obj);
                                    }
                                    $(selector).find('.application-box').removeClass('active');
                                    $(selector).find('.main-menu').addClass('d-none');
                                }
                            }
                        }).sortable({
                            revert: false
                        });
                    }, 1000);
                    // END: Drag and Drop Application  

                    // START: Right click on application
                    $.contextMenu({
                        selector: '.editable.type-one',
                        callback: function callback(key, options) {
                            var m = "clicked: " + key;
                            //window.console && console.log(m) || alert(m);
                            var id_elem = $(this)[0].id;

                            $(selector).find('.' + id_elem).remove();
                        },
                        items: {
                            "delete": { name: "Delete", icon: "delete" }
                        }
                    });

                    $.contextMenu({
                        selector: '.editable.type-two',
                        callback: function callback(key, options) {
                            var m = "clicked: " + key;
                            //window.console && console.log(m) || alert(m);
                            var id_elem = $(this)[0].id;

                            $(selector).find('.' + id_elem).remove();
                        },
                        items: {
                            "add": { name: "Open in new window", icon: "add" },
                            "delete": { name: "Delete", icon: "delete" }
                        }
                    });

                    $('.context-menu-one').on('click', function (e) {
                        console.log('clicked', this);
                    });
                    // END: Right click on application

                    // START: Click any where to close main menu
                    $('#editor-container').click(function (e) {
                        if (e.target != $(selector).find('.main-menu')[0]) {
                            $(selector).find('.application-box').removeClass('active');
                            $(selector).find('.main-menu').addClass('d-none');
                        }
                    });
                    // END: Click any where to close main menu

                    // START: Double click and open window
                    $.each($('.application-item.editable'), function (i, e) {
                        var id_elem = $(e)[0].id;
                        $(e).dblclick(function () {
                            //open window
                            $(selector).find('.application-window.' + id_elem).removeClass('d-none');
                            $(".application-window." + id_elem + "").draggable({
                                cursor: 'move'
                            }).resizable({
                                // maxHeight: 450,
                                // maxWidth: 800,
                                minHeight: 600,
                                minWidth: 670
                            });
                            $(selector).find('.' + id_elem + '.opened-windows');
                            $("." + id_elem + ".opened-windows").removeClass('d-none').addClass('active');
                        });

                        $(".application-window." + id_elem + "").find('.close').click(function (e) {
                            $(".application-window." + id_elem + "").addClass('d-none');
                            $("." + id_elem + ".opened-windows").addClass('d-none').removeClass('active');
                        });
                    });
                    // END: Double click and open window
                },
                methods: {
                    clickMenu: function clickMenu(e) {
                        $(selector).find('#main-menu').toggleClass('active');
                        $(selector).find('.main-menu').toggleClass('d-none');
                    }
                }
            });
        }
    }]);

    return AddRemoveApplication;
}();

exports.default = AddRemoveApplication;

},{}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ToolTips = function () {
    function ToolTips() {
        _classCallCheck(this, ToolTips);

        this.init();
    }

    _createClass(ToolTips, [{
        key: "init",
        value: function init() {}
    }]);

    return ToolTips;
}();

exports.default = ToolTips;

},{}],3:[function(require,module,exports){
'use strict';

var _tooltips = require('./component/tooltips');

var _tooltips2 = _interopRequireDefault(_tooltips);

var _addRemoveApplication = require('./component/add-remove-application');

var _addRemoveApplication2 = _interopRequireDefault(_addRemoveApplication);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var tooltips = new _tooltips2.default();
var addremove_application = new _addRemoveApplication2.default();

},{"./component/add-remove-application":1,"./component/tooltips":2}]},{},[3]);
