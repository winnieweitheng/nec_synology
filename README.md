# NEC_Synology

Develop and reproduce the UI for Synology (https://www.synology.com/en-global/dsm/6.2)

Visit the demo: https://nostalgic-torvalds-4ccfbf.netlify.com/

**Technologies**

1. Sass - CSS preprocessor
1. jQuery - Library
1. Vue.js - Progressive framework for building user interfaces
1. Gulp - Streaming build system in front-end web development
1. jquery.contextMenu - CTA for mouse right click
1. jquery-ui - Draggable / Droppable / Resizable / Sortable

**Development Environment Setup**
To setup development environment, do the following steps:

1. $ git clone https://<username>@bitbucket.org/<username>/nec_synology.git
2. $ npm install
1. $ gulp

**Production Setup**
To create production folder, do the following steps:
HTML files with all references will be generated in dist folder

1. $ gulp build

**Folder Structure**

1. app => development folder
1. assets => compilation files with gulp task from app folder, for development purpose
1. dist => production folder

**Credit**

Credit to the icon developers from flaticon. https://www.flaticon.com