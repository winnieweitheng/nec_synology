var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var svgSymbols = require('gulp-svg-symbols');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');

// For development
gulp.task('default', function (callback) {
    runSequence(['html','js','sass', 'img', 'node_modules','vendor','dummy-data', 'browserSync', 'watch'],
        callback
    )
})

// For production
gulp.task('build', function (callback) {
    runSequence('clean:dist',
        ['useref'],['images-prod'],['dummy-data-prod'],['svg-prod'],
        callback
    )
})

// Compile Sass to CSS
gulp.task('sass', function () {
    return gulp.src('app/scss/*.scss') // Gets all files ending with .scss in app/scss and children dirs
        .pipe(sass())
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// to handle js
gulp.task('js', function() {
	browserify('app/js/main.js')
		.transform(babelify)
		.bundle()
		.pipe(source('main.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Gulp watch syntax 
// ('watch', ['array', 'of', 'tasks', 'to', 'complete','before', 'watch']
gulp.task('watch', ['browserSync', 'sass'], function () {
    // ('watch the files', ['tasks','to','run'])
    gulp.watch('app/scss/**/*.scss', ['sass']); // watch sass file
    gulp.watch('app/**/*.html', ['html']); // watch html file
    gulp.watch('app/js/**/*.js', ['js']); // watch js file
    gulp.watch('app/dummy-data//*.json', ['dummy-data']); // watch js file
});

// Live reloading browsers
gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: 'assets'
        },
    })
})

// Concatenates css and js
// gulp.task('useref', function(){
//     return gulp.src(['assets/index.html'])
//         // .pipe($.useref({searchPath: ['assets', 'app', '.']}))
//         //minifies only if it's a JavaScript file
//         .pipe(gulpIf('*.js', uglify()))
//         //minifies only if it's a CSS file
//         .pipe(gulpIf('*.css', cssnano()))
//         .pipe(gulp.dest('dist'))
// });

gulp.task('useref', function () {
    return gulp.src('assets/index.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify())) // Minifies if only js files
        .pipe(gulpIf('*.css', cssnano())) // Minifies if only css files
        .pipe(gulp.dest('dist'))
})

// Minify images
gulp.task('images', function () {
    return gulp.src('app/images/**/*.+(png|jpg|gif|svg)')
        .pipe(cache(imagemin({
            // Setting interlaced to true
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'))
});

// Copy fonts to dist
gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
})

// Cleaning up generated files automatically
gulp.task('clean:dist', function () {
    return del.sync('dist');
})

// svg symbols
gulp.task('svg', function () {
    return gulp.src('app/images/svg/*.svg')
        .pipe(svgSymbols())
        .pipe(gulp.dest('app/images'))
})

// copy node_modules
gulp.task('node_modules', function () {
    return gulp.src(['./node_modules/**/*.js'])
        .pipe(gulp.dest('assets/node_modules'))
})

// copy vendor
gulp.task('vendor', function () {
    return gulp.src(['vendor/**/*.js'])
        .pipe(gulp.dest('assets/vendor'))
})

// copy dummy data
gulp.task('dummy-data', function () {
    return gulp.src(['app/dummy-data/*.json'])
        .pipe(gulp.dest('assets/dummy-data'))
        .pipe(browserSync.reload({
            stream: true
        }))
})

// copy html
gulp.task('html', function () {
    return gulp.src(['app/*.html'])
        .pipe(gulp.dest('assets'))
        .pipe(browserSync.reload({
            stream: true
        }))
})

// copy image
gulp.task('img', function() {
    return gulp.src(['app/images/*.+(png|jpg|gif|svg|css)'])
        .pipe(gulp.dest('assets/images'))
})

// For Production
gulp.task('images-prod', function(){
    return gulp.src('assets/images/**/*')
    // caching images that ran through imagemin
    .pipe(cache(imagemin({
        //setting interlaced to true
        interlaced: true
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('svg-prod', function(){
    return gulp.src('assets/images/*.svg')
    .pipe(gulp.dest('dist/images'));
});

// copy dummy data
gulp.task('dummy-data-prod', function () {
    return gulp.src(['assets/dummy-data/*.json'])
        .pipe(gulp.dest('dist/dummy-data'))
        .pipe(browserSync.reload({
            stream: true
        }))
})